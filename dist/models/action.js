function defaultReducer() {
  const actionList = actions;
  return function (state = initialState, action) {
    for (const key in actionList) {
      const actionTest = actionList[key];

      if (actionTest.success === action.type) {
        return actionTest.onSuccess(state, action);
      } else if (actionTest.error === action.type) {
        return actionTest.onError(state, action);
      }
    }

    return state;
  };
}
/**
 * 
 * @param {string} slice 
 * @param {string} name 
 * @param {function} action 
 * @param {function} saga 
 * @param {function} onSuccess 
 * @param {function} onError 
 * @param {function} reducer 
 * @returns {object}
 */


export default function (slice, name, action, saga, onSuccess, onError, reducer) {
  slice = slice.toUpperCase();
  name = `${slice}_${name.toUpperCase()}`;
  reducer = reducer || defaultReducer;
  return {
    get slice() {
      return slice;
    },

    get type() {
      return name;
    },

    get success() {
      return `${name}_SUCCESS`;
    },

    get error() {
      return `${name}_ERROR`;
    },

    get action() {
      return action;
    },

    get saga() {
      return saga;
    },

    get reducer() {
      return reducer;
    },

    onSuccess,
    onError
  };
}