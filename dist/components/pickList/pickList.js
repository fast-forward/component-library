function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import PropTypes from 'prop-types';
import "./pickList.scss";
import { FaCheck, FaPlus, FaTimes } from 'react-icons/fa';
import { bem } from '../../helpers';

class PickList extends React.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "setTemplate", index => {
      if (index !== null && index !== "") {
        this.setState({
          activeIndex: index
        });
      }
    });

    _defineProperty(this, "onAdd", () => {
      var item = { ...this.props.data[this.state.activeIndex]
      };

      if (typeof this.props.onAdd == 'function') {
        this.props.onAdd(item);
      }

      this.onClose();
    });

    _defineProperty(this, "onClose", () => {
      this.setState({
        isActive: false,
        activeIndex: -1
      }, () => {
        if (typeof this.props.onClose == 'function') {
          this.props.onClose();
        }
      });
    });

    _defineProperty(this, "onOpen", () => {
      this.setState({
        isActive: true
      }, () => {
        if (typeof this.props.onOpen == 'function') {
          this.props.onOpen();
        }
      });
    });

    this.state = {
      isActive: false,
      activeIndex: -1
    };
  }

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, Array.isArray(this.props.data) && this.props.data.length > 0 && /*#__PURE__*/React.createElement("div", {
      className: bem('pick-list', null, [this.state.isActive ? 'open' : 'closed'], this.props.class)
    }, !this.state.isActive && /*#__PURE__*/React.createElement("button", {
      size: "field",
      className: bem('pick-list', 'add'),
      onClick: this.onOpen
    }, this.props.useIcons && /*#__PURE__*/React.createElement(FaPlus, null), this.props.useLabels && /*#__PURE__*/React.createElement("span", null, this.props.addLabel)), this.state.isActive && /*#__PURE__*/React.createElement("div", {
      className: this.props.isModal ? bem('pick-list', 'modal') : null
    }, /*#__PURE__*/React.createElement("button", {
      size: "field",
      disabled: this.state.activeIndex === -1,
      className: bem('pick-list', 'create'),
      onClick: () => this.onAdd()
    }, this.props.useIcons && /*#__PURE__*/React.createElement(FaCheck, null), this.props.useLabels && /*#__PURE__*/React.createElement("span", null, this.props.createLabel)), /*#__PURE__*/React.createElement("select", {
      id: "itemList",
      className: bem('pick-list', 'list'),
      value: this.state.activeIndex,
      onChange: event => this.setTemplate(parseInt(event.target.value))
    }, /*#__PURE__*/React.createElement("option", {
      value: "-1"
    }, this.props.listLabel), this.props.data.map((listItem, index) => /*#__PURE__*/React.createElement("option", {
      key: index,
      value: index
    }, this.props.labelCallBack ? this.props.labelCallBack(listItem) : listItem.label))), /*#__PURE__*/React.createElement("button", {
      size: "field",
      kind: "secondary",
      className: bem('pick-list', 'close'),
      onClick: () => this.onClose()
    }, this.props.useIcons && /*#__PURE__*/React.createElement(FaTimes, null), this.props.useLabels && /*#__PURE__*/React.createElement("span", null, this.props.closeLabel)))));
  }

}

PickList.defaultProps = {
  addLabel: "Add",
  createLabel: "Create",
  closeLabel: "Close",
  listLabel: "Choose",
  className: "",
  isModal: false,
  onClose: null,
  onOpen: null,
  useIcons: false,
  useLabels: true
};
PickList.propTypes = {
  data: PropTypes.array.isRequired,
  addLabel: PropTypes.string,
  createLabel: PropTypes.string,
  closeLabel: PropTypes.string,
  listLabel: PropTypes.string,
  className: PropTypes.string,
  onAdd: PropTypes.func.isRequired,
  isModal: PropTypes.bool,
  useIcons: PropTypes.bool,
  useLabels: PropTypes.bool
};
export default PickList;