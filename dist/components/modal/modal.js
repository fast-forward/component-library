function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import PropTypes from 'prop-types';
import "./modal.scss";
import { FaTimes } from 'react-icons/fa';
import Draggable from 'react-draggable';
import { bem } from '../../helpers';
/**
 * Displays child elements within a modal
 */

export default class Modal extends React.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onClose", e => {
      if (this.props.onClose) {
        this.props.onClose(e, this);
      }
    });
  }

  render() {
    const bg = /*#__PURE__*/React.createElement("div", {
      className: bem('modal', 'bg'),
      onClick: this.onClose
    });
    const content = /*#__PURE__*/React.createElement("div", {
      className: bem('modal', 'content')
    }, /*#__PURE__*/React.createElement("div", {
      className: bem('modal', 'header')
    }, /*#__PURE__*/React.createElement("label", {
      className: bem('modal', 'title')
    }, this.props.title), /*#__PURE__*/React.createElement("button", {
      className: bem('modal', 'close', [], 'btn'),
      onClick: this.onClose
    }, /*#__PURE__*/React.createElement(FaTimes, null))), /*#__PURE__*/React.createElement("div", {
      className: bem('modal', 'body')
    }, this.props.children));

    if (this.props.dragSettings) {
      return /*#__PURE__*/React.createElement("div", {
        className: bem('modal', null, [this.props.show ? 'visible' : 'hidden'], this.props.className)
      }, bg, /*#__PURE__*/React.createElement(Draggable, this.props.dragSettings, /*#__PURE__*/React.createElement("div", {
        className: "draggable_container"
      }, content)));
    } else {
      return /*#__PURE__*/React.createElement(React.Fragment, null, bg, /*#__PURE__*/React.createElement("div", {
        className: bem('modal', null, [this.props.show ? 'visible' : 'hidden'], this.props.className)
      }, content));
    }
  }

}

_defineProperty(Modal, "defaultProps", {
  dragSettings: null,
  className: "",
  show: false,
  title: ""
});

_defineProperty(Modal, "propTypes", {
  dragSettings: PropTypes.object,
  className: PropTypes.string,
  title: PropTypes.string,
  show: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired
});