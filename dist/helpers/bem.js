/**
 * 
 * @param {string} block 
 * @param {string} element 
 * @param {array} modifiers
 * @param {string} append 
 */
export default function (block, element = '', modifiers = [], append = '') {
  let be = block.trim();

  if (element) {
    be += `__${element.trim()}`;
  }

  return [be, ...modifiers.filter(m => !!m).map(m => `${be}--${m}`), append].filter(m => !!m).join(' ');
}