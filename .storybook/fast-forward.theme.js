import { create } from '@storybook/theming/create';

const colors = {
    black: 'rgb(0, 0, 0)',
    darkGray: 'rgb(40, 48, 54)',
    gray: 'rgb(127, 127, 127)',
    lightGray: 'rgb(210, 210, 210)',
    white: 'rgb(255, 255, 255)',
    red: 'rgb(223, 67, 68)'
};

export default create({
    base: 'light',

    colorPrimary: colors.black,
    colorSecondary: colors.red,

    // UI 
    appBg: colors.black,
    appContentBg: colors.white,
    //appBorderColor: colors.darkGray,
    appBorderRadius: '0.5em',

    // Typography
    fontCode: 'monospace',
    fontBase: '"lato", "HelveticaNeue", "Helvetica Neue", "Helvetica-Neue", Helvetica, Arial, sans-serif',
    
    // Text colors
    textColor: colors.gray,
    textInverseColor: colors.gray,

    // Toolbar default and active colors
    barTextColor: colors.lightGray,
    barSelectedColor: colors.red,
    barBg: colors.darkGray,

//   // Form colors
    inputBg: colors.darkGray,
    inputBorder: colors.lightGray,
    inputTextColor: colors.white,
    inputBorderRadius: '0.5em',

    brandTitle: 'Fast Forward - React Component Library',
    brandUrl: 'https://fastforward.sh',
    brandImage: 'http://www.fastforward.sh/wp-content/uploads/2015/04/ff_logo_340x501.png',
});