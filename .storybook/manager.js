import { addons } from '@storybook/addons';
import theme from './fast-forward.theme';

addons.setConfig({
  theme: theme,
});