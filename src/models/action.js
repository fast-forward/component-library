/**
 * default reducer behvior for mapping actions->reducers.
 * @returns {function}
 */
function defaultReducer() {
    const actionList = actions
    return function (state = initialState, action) {
        for (const key in actionList) {
            const actionTest = actionList[key]
            if (actionTest.success === action.type) {
                return actionTest.onSuccess(state, action)
            } 
            else if (actionTest.error === action.type) {
                return actionTest.onError(state, action)
            }
        }
    
        return state
    }  
}
/**
 * model action and its complimentary saga, reducer etc. together. creates all event names and watchers. 
 * @param {string} slice name of slice this action is associated with (user, data)
 * @param {string} name action name (login, getData)
 * @param {function} action returns redux action
 * @param {function} saga returns saga
 * @param {function} onSuccess reducer function on success of saga execution
 * @param {function} onError reducer function on failure of saga execution 
 * @param {function} reducer optional reducer code that maps actions to reducers
 * @returns {object}
 */
export default function (slice, name, action, saga, onSuccess, onError, reducer) {
    slice = slice.toUpperCase()
    name = `${slice}_${name.toUpperCase()}`
    reducer = reducer || defaultReducer
    
    return {
        get slice() {return slice},
        get type() {return name},
        get success() {return `${name}_SUCCESS`},
        get error() {return `${name}_ERROR`},
        get action() {return action},
        get saga() {return saga},
        get reducer() {return reducer},
        onSuccess,
        onError
    }   
}