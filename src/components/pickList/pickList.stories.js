import React from 'react';
import PickList from './pickList';
import { withKnobs, boolean, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import '../../_stories/stories.scss';

export default {
    title: "Components/PickList", 
    decorators: [withKnobs],
    component: PickList
  };

// Knobs as dynamic variables.
export const Dynamic = () => {

  const params = {
    useIcons: boolean("Use Icons", true),
    useLabels: boolean("Use Labels", true),
    data: object("List", [
        {
          label: "Choice 1",
          value: 1
        },
        {
          label: "Choice 2",
          value: 2
        },
        {
          label: "Choice 3",
          value: 3
        }
    ])
  }

  const actions = {
    onAdd: action('onAdd'),
    onClose: action('onClose'),
    onOpen: action('onOpen'),
  };

  return <PickList {...params} {...actions} />
};