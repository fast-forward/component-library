import React from 'react';
import PropTypes from 'prop-types';
import "./pickList.scss";
import { FaCheck, FaPlus, FaTimes } from 'react-icons/fa';
import {bem}from '../../helpers'

class PickList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isActive: false,
            activeIndex: -1
        };
    }

    setTemplate = index => {
        if (index !== null && index !== "") {
            this.setState({ activeIndex: index});
        }
    };

    onAdd = () => {
        var item = {...this.props.data[this.state.activeIndex]};
        if (typeof this.props.onAdd == 'function') {
            this.props.onAdd(item);
        }
        this.onClose();
    };

    onClose = () => {
        this.setState({
            isActive: false,
            activeIndex: -1
        }, () => {
            if (typeof this.props.onClose == 'function') {
                this.props.onClose();
            }
        });
    };

    onOpen = () => {
        this.setState({
            isActive: true
        }, () => {
                if (typeof this.props.onOpen == 'function') {
                    this.props.onOpen();
                }
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                { Array.isArray(this.props.data) && this.props.data.length > 0 &&
                    <div className={bem('pick-list', null, [this.state.isActive? 'open' : 'closed'], this.props.class)}>
                        {!this.state.isActive &&
                            <button size="field" className={bem('pick-list','add')} onClick={this.onOpen}>
                                {this.props.useIcons && 
                                    <FaPlus />
                                }
                                {this.props.useLabels && 
                                    <span>{this.props.addLabel}</span> 
                                }
                            </button>
                        }

                        {this.state.isActive &&
                            <div className={this.props.isModal ? bem('pick-list', 'modal') : null}>
                                <button size="field" disabled={this.state.activeIndex === -1} className={bem('pick-list','create')} onClick={() => this.onAdd()}>
                                    {this.props.useIcons && 
                                        <FaCheck />
                                    } 
                                    {this.props.useLabels && 
                                        <span>{this.props.createLabel}</span> 
                                    }
                                </button>
                                <select 
                                    id="itemList" 
                                    className={bem('pick-list','list')}
                                    value={this.state.activeIndex} 
                                    onChange={ event => this.setTemplate(parseInt(event.target.value)) }
                                >
                                    <option value="-1">{this.props.listLabel}</option>

                                    { this.props.data.map( (listItem, index) => 
                                        <option 
                                            key={index} 
                                            value={index}
                                        >
                                            {this.props.labelCallBack ? this.props.labelCallBack(listItem) : listItem.label}
                                        </option>
                                    )}
                                </select>
                                <button size="field" kind="secondary" className={bem('pick-list','close')}  onClick={ () => this.onClose() }>
                                    {this.props.useIcons && 
                                        <FaTimes />
                                    }
                                    {this.props.useLabels && 
                                        <span>{this.props.closeLabel}</span> 
                                    }
                                 </button>
                            </div>
                        }
                    </div>
                }
            </React.Fragment>
        );
    }
}

PickList.defaultProps = {
    addLabel: "Add",
    createLabel: "Create",
    closeLabel: "Close",
    listLabel: "Choose",
    className: "",
    isModal: false,
    onClose: null,
    onOpen: null,
    useIcons: false,
    useLabels: true
}

PickList.propTypes = {
    data: PropTypes.array.isRequired,
    addLabel: PropTypes.string,
    createLabel: PropTypes.string,
    closeLabel: PropTypes.string,
    listLabel: PropTypes.string,
    className: PropTypes.string,
    onAdd: PropTypes.func.isRequired,
    isModal: PropTypes.bool,
    useIcons: PropTypes.bool,
    useLabels: PropTypes.bool
};

export default PickList;