import React from 'react';
import { withKnobs, boolean, object, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import FilterList from './filterList';
import {facetOptions, filters} from "../../_stories/defaults";
import '../../_stories/stories.scss';

export default {
    title: "Components/FilterList",
    decorators: [withKnobs],
    component: FilterList
  };

// Knobs as dynamic variables.
export const Dynamic = () => {

  const params = {
    horizontal: boolean("Horizontal", true),
    className: text("Class Name", ""),
    clearAllLabel: text("Clear All label", "Clear All"),
    filters: object("Filters", filters),
    options: object("Options", facetOptions)
  }

  const actions = {
    onRemove: action('onRemove'),
    onClear: action('onClear')
  };

  return <FilterList {...params} {...actions} />
};