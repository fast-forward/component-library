import React from 'react';
import PropTypes from 'prop-types';
import "./filterList.scss";
import {bem}from '../../helpers'

class FilterList extends React.Component {
    render() {
        var keys = Object.keys(this.props.filters);
        var filtersArr = [];

        keys.map( key => {
            var valuesArr = this.props.filters[key] || [];

            return valuesArr.map( value => {
                return filtersArr.push({
                    name: key,
                    value: value
                })
            })
        });

        return (
            <div className={bem('filter-list', null, [this.props.horizontal ? 'horizontal' : 'vertical'],  this.props.className)}>
                <div className={bem('filter-list','list')}>

                    {filtersArr.map( (filter, index) => (
                        <button 
                            key={ filter.name + "_" + filter.value} 
                            className={bem('filter', null, [filter.value])} 
                            onClick={() => this.props.onRemove(filter, index)}
                        >
                                {this.props.options[filter.name] ? this.props.options[filter.name].label + ": " : ""} {filter.value}
                        </button>
                    ))}
                    {filtersArr.length > 0 &&
                        <button className={bem('filter', null, ['clear-all'])} onClick={() => this.props.onClear()}>{this.props.clearAllLabel}</button>
                    }
                </div>
            </div>
        )
    }
}

FilterList.propTypes = {
    className: PropTypes.string,
    horizontal: PropTypes.bool,
    clearAllLabel: PropTypes.string,
    onRemove: PropTypes.func.isRequired,
    onClear: PropTypes.func.isRequired,
    filters: PropTypes.object.isRequired,
    options: PropTypes.object.isRequired
};

FilterList.defaultProps = {
    className: "",
    horizontal: false,
    clearAllLabel: "Clear All"
}

export default FilterList;
