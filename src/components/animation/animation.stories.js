
import React from 'react';
import { withKnobs, boolean, object, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Animation from './animation';
import {facetOptions, filters, facets} from "../../_stories/defaults";
import {TweenMax} from "gsap";
//import GSDevTools from "gsap/GSDevTools";
import '../../_stories/stories.scss';

export default {
    title: "Components/Animation",
    decorators: [withKnobs],
    component: Animation,
  };

// Knobs as dynamic variables.
export const SlideIn = () => {
  const show = boolean("Show", false); 
  return (
    <Animation
      className="verticle-list" 
      active={boolean("Show", false)}
      debug={boolean("Debug", false)}
      onReady={ element => {
        
      }}
      onEntering={ (element, done) => {
        let animLength = .4;
        TweenMax.staggerTo(element.children, animLength, {
          x: 0,
          scale: 1,
          marginBottom: '0.4em',
          backgroundColor: "#ccc",
          onComplete: () => {
            done();
          }
        }, animLength / element.children.length);
        
      }}
      onEntered={action("onEntered")}
      onExiting={ (element, done) => {

        let animLength = .4;

        TweenMax.staggerTo(element.children, animLength, {
          x: '-8em',
          scale: 0,
          marginBottom: 0,
          backgroundColor: "#888",
          onComplete: () => {
            done();
          }
        }, animLength / element.children.length );

      }}
      onExited={action("onExited")}
    >
      
      <div>Hello world</div>
      <div>Goodbye earth</div>
      <div>Hello world</div>
      <div>Goodbye earth</div>
      <div>Hello world</div>
      <div>Goodbye earth</div>
      <div>Hello world</div>
      <div>Goodbye earth</div>

    </Animation>
  )
};

export const Bounce = () => {
  const show = boolean("Show", false); 

  return (
    
    <Animation
      className="bounce" 
      active={boolean("Show", false)}
      debug={boolean("Debug", false)}
      onReady={ (element, trigger) => {
       
      }}
      onEntering={ (element, done) => {
        let animLength = .4;

        TweenMax.staggerTo(element.children, animLength, {
          y: 0,
          scale: 1,
          marginLeft: '0.4em',
          backgroundColor: "#ccc",
          onCompleteAll: done
        }, animLength / element.children.length);
        
      }}
      onEntered={action("onEntered")}
      onExiting={ (element, done) => {

        let animLength = .4;

        TweenMax.staggerTo(element.children, animLength, {
          y: '-3em',
          scale: 0,
          marginLeft: 0,
          backgroundColor: "#888",
          onCompleteAll: done
        }, animLength / element.children.length );

      }}
      onExited={action("onExited")}

    >
      
      <div>Hello world</div>
      <div>Goodbye earth</div>
      <div>Hello world</div>
      <div>Goodbye earth</div>
      <div>Hello world</div>

    </Animation>
  )
};