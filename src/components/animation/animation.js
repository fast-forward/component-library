import React from 'react';
import {bem}from '../../helpers'
import "./animation.scss";
//import PropTypes from 'prop-types';

class Animation extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            event: undefined,
            ready: false,
            busy: true
        }
        this.element = undefined;
    }

    //EVENTS
    isReady = () => !!(this.state.ready && this.element );

    setElement = el => {
        this.element = el; 

        this.setState({ready: true, busy: false}, () => this.setEvent("ready", () => this.props.onReady(this.element, this.trigger)));
    };

    setEvent = (name, cb, done) => {
        this.setState({event: name}, () => {
            if (cb) {
                cb(this.element, () => {
                    done();
                });
            } else {
                done();
            }
        }
    )};

    trigger = eventName => {
        if (['enter', 'exit', 'done'].indexOf(eventName.toLowerCase()) > -1) {
            return;
        }

        let prefix = `on${eventName.charAt(0).toUpperCase()}${eventName.slice(1)}`

        if (this.props[`${prefix}ing`]) {
            this.setEvent (
                `${eventName}ing`,
                this.props[`${prefix}ing`],
                this.done,
                () =>  {
                    if (this.props[`${prefix}ed`]) {
                        this.setEvent (
                            `${prefix}ed`, 
                            this.props[`${prefix}ed`]
                        )
                    }
                }
            )
        } else if (this.props[`${prefix}ed`]) {
            this.setEvent (
                `${prefix}ed`, 
                this.props[`${prefix}ed`]
            )
        }
    }

    enter = () => {
        this.setEvent (
            "entering",
            this.props.onEntering,
            () => {
                this.setEvent (
                    "entered", 
                    this.props.onEntered
                );
            }
        );
    }

    exit = () => {
        
        this.setEvent (
            "exiting",
            this.props.onExiting,
            () => {
                this.setEvent (
                    "exited", 
                    this.props.onExited
                );
            }
        );
    }

    //COMPONENT LIFECYCLE
    componentDidMount = () => {

    }
    
	componentDidUpdate = (prevProps, prevState) => {
        if (this.isReady() && this.props.active !== prevProps.active) {
            if (this.props.active) {
                this.enter();
            } else {
                this.exit();
            }
        }
    }
    
	componentWillUnmount = () => {

    }

    debugInfo (name, value) {
        return (
            <fieldset className={bem('debug','info')}>
                <label>{name}:</label> <span>{value}</span>
            </fieldset>
        )
    }

    render() {
        return (
            <div className={bem('animation', null,[this.state.event, this.isReady() && 'ready', this.props.debug && 'debug'], this.props.className)} >
                {this.props.debug &&
                    <div className={bem('debug')}>
                        {this.debugInfo('event', this.state.event)}
                        {this.debugInfo('active', this.props.active.toString())}
                        {this.debugInfo('ready', this.isReady().toString())}
                        {this.debugInfo('busy', this.state.busy.toString())}
                        {typeof this.props.debug === Object && Object.keys(this.props.debug).map( key => this.debugInfo(key, this.props.debug[key]) )}
                    </div>
                }
                <div 
                    className={bem('stage')}
                    children={this.props.children}
                    ref={ (el) => {
                        if (!this.isReady()) {
                            this.setElement(el);
                        }
                    }}
                />
            </div>
        )
    }
}

// Animation.propTypes = {
// };

// Animation.defaultProps = {
// }

export default Animation;
