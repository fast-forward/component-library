import React from 'react';
import { withKnobs, boolean, object, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import FacetList from './facetList';
import {facetOptions, filters, facets} from "../../_stories/defaults";
import '../../_stories/stories.scss';

export default {
    title: "Components/FacetList",
    decorators: [withKnobs],
    component: FacetList
  };

// Knobs as dynamic variables.
export const Dynamic = () => {

    const params = {
      horizontal: boolean("Horizontal", true),
      className: text("Class Name", ""),
      filters: object("Filters", filters),
      facets: object("Facets", facets),
      options: object("Options", facetOptions)
    }

    const actions = {
      onRemove: action('onRemove'),
      onClear: action('onClear'),
      onChange: action('onChange')
    };

    return <FacetList {...params} {...actions} />
};