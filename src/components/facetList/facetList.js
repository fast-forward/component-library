import React from 'react';
import PropTypes from 'prop-types';
import "./facetList.scss";
import {bem}from '../../helpers'
class FacetList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            facetGroupVisibility: [],
            contextualFilters: {}
        };

        this.configured = false;
    }
  
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        this.configure();
    }
    
    configure = () => {
        if (!this.configured) {
            try {
                Object.keys(this.props.options).map( key => {
                    var facet = this.props.options[key];

                    if (facet.expanded) {
                        if (!this.isFacetGroupVisible(facet.name)) {
                            this.toggleFacetGroupVisibility(facet.name)
                        }
                    }
                    return key;
                });

                this.configured = true;
            } catch (error) {
                
            }  
        }
    }

    getFacetValueUi = (buckets, facetFieldName, facetFieldLabel) => {
        var values = buckets.filter(facet => {
            return (facet && facet.val && (!this.state.contextualFilters[facetFieldName] || facet.val.toLowerCase().indexOf(this.state.contextualFilters[facetFieldName]) > -1))
        });

        if (values.length > 0) {
            return values.map(facet => {
                if (facet.val && facet.val !== null && facet.val !== "" && facet.count !== "0" && facet.count !== 0) {
                    var label = facet.val;
                    return (
                        <div key={facetFieldName + "-" + facet.val} className={bem('facet','value')}>
                            <input 
                                id={facetFieldName + "--" + facet.val} 
                                name={facetFieldName} 
                                value={facet.val} 
                                type="checkbox" 
                                checked={this.isFilterChecked(facetFieldName, facet.val)} 
                                onChange={ event => {
                                    let eventData = {
                                        name: facetFieldName,
                                        value: facet.val,
                                        active: event.target.checked
                                    };

                                    this.props.onChange(eventData);
                                }}
                            />
                            <label htmlFor={facetFieldName + "--" + facet.val}>{label} ({facet.count})</label>
                        </div>
                    )
                } else {
                    return null;
                }
            })
        } else {
            return <div className={bem('facet','item' , ['no-results'])}>No matches found in {facetFieldLabel}</div>
        }
            
    }

    toggleFacetGroupVisibility = facetGroup => {
        var i = this.state.facetGroupVisibility.indexOf(facetGroup)
        if (i > -1) {
            this.state.facetGroupVisibility.splice(i, 1);
        } else {
            this.state.facetGroupVisibility.push(facetGroup);
        }

        this.setState({ facetGroupVisibility: this.state.facetGroupVisibility });
    }

    isFacetGroupVisible = facetGroup => {
        return this.state.facetGroupVisibility.indexOf(facetGroup) > -1;
    }

    isFilterChecked = (name, value) => {
        return (typeof this.props.filters === "object" && this.props.filters[name] && this.props.filters[name].indexOf(value.toString()) > -1);
    }

    render() {
        if (this.props.facets && this.props.options) {
            return (
                <div className={bem(
                    'facet-list', 
                    null, 
                    [this.props.horizontal ? 'horizontal' : 'vertical'], 
                    this.props.className
                )}>
                    { Object.keys(this.props.options).map(facetFieldName => {

                        let facetUI = this.props.options[facetFieldName],
                            facetData = this.props.facets[facetFieldName],
                            show = true,
                            fields = Object.keys(facetUI.filters);

                        if (fields && fields.length) {
                            show = false;

                            fields.map( fieldName => {
                                if (!show) {
                                    try {
                                        let selectedValues = this.props.filters[fieldName],
                                            neededValues = facetUI.filters[fieldName],
                                            intersection = neededValues.filter(x => selectedValues.includes(x));
                                        show = (intersection.length > 0);
                                    } catch (error) {
                                        
                                    }
                                }
                                return fieldName;
                            });
                        }

                        if (show && facetUI && facetData && facetData.buckets && facetData.buckets.length) {
                            let isVisible = this.isFacetGroupVisible(facetFieldName);
                            return (
                                <div key={facetFieldName} className={bem('facet', null,  [isVisible ? 'active' : ''])} onMouseLeave={ event => {
                                    if (this.props.horizontal && isVisible) {
                                        this.toggleFacetGroupVisibility(facetFieldName);
                                    }
                                }}>
                                    <div 
                                        className={bem('facet', 'heading')}
                                        onClick={() => this.toggleFacetGroupVisibility(facetFieldName)}
                                    >
                                        {facetUI.label}

                                        {/* <svg className="svg-icon-size svg-icons_angle-down-dims">
                                            <use xlinkHref="#icons_angle-down" />
                                        </svg> */}

                                    </div>

                                    {isVisible && facetData.buckets.length >= 10 &&
                                        <div className={bem('facet','contextual-filter')}>
                                            <input 
                                                placeholder={"Search " + facetUI.label} 
                                                type="search" 
                                                onChange={event => this.onContextualFilterChange(facetFieldName, event)} 
                                            />
                                        </div>
                                    }

                                    <div className={bem('facet','values', ['check-boxes'])}>
                                        {this.getFacetValueUi(facetData.buckets, facetFieldName, facetUI.label)}
                                    </div>
                                </div>
                            );
                        } else {
                            return null;
                        }
                    })}
                </div>
            )

        } else {
            return null;
        }
    }
}

FacetList.propTypes = {
    className: PropTypes.string,
    horizontal: PropTypes.bool,
    options: PropTypes.object.isRequired,
    facets: PropTypes.object.isRequired,
    filters: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

FacetList.defaultProps = {
    className: "",
    horizontal: false
}

export default FacetList;
