import React from 'react';
import PropTypes from 'prop-types';
import "./modal.scss";
import { FaTimes } from 'react-icons/fa';
import Draggable from 'react-draggable';
import {bem}from '../../helpers'

/**
 * Displays child elements within a modal
 */
export default class Modal extends React.Component {

    static defaultProps = {
        dragSettings: null,
        className: "",
        show: false,
        title: ""
    };
    
    static propTypes = {
        dragSettings: PropTypes.object,
        className: PropTypes.string,
        title: PropTypes.string,
        show: PropTypes.bool.isRequired,
        children: PropTypes.node.isRequired,
        onClose: PropTypes.func.isRequired
    };

    /**
     * Event handler responsible for relaying events to props.onClose
     * @param {Event} e 
     */
    onClose = e => {
        if (this.props.onClose) {
            this.props.onClose(e, this);
        }
    }

    render() {
        
        const bg = (
            <div className={bem('modal','bg')} onClick={this.onClose}></div>
        )

        const content = (
            <div className={bem('modal','content')}>
                <div className={bem('modal','header')}>
                    <label className={bem('modal','title')}>{this.props.title}</label>
                    <button className={bem('modal','close', [], 'btn')} onClick={this.onClose}>
                        <FaTimes />
                    </button>
                </div>
                <div className={bem('modal','body')}> 
                    {this.props.children}
                </div>
            </div>
        )

        if (this.props.dragSettings) {
            return (
                <div className={bem('modal', null, [this.props.show ? 'visible' : 'hidden'], this.props.className)}>
                    {bg}
                    <Draggable {...this.props.dragSettings}>
                        <div className="draggable_container">
                            {content}
                        </div>
                    </Draggable>
                </div>
            )
        } else {
            return (
                <React.Fragment>
                    {bg}
                    <div className={bem('modal', null, [this.props.show ? 'visible' : 'hidden'], this.props.className)}>
                        {content}
                    </div>
                </React.Fragment>
            )
        }
    }
}

