import React from 'react';
import Modal from './modal';
import { withKnobs, boolean, text, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import '../../_stories/stories.scss';

export default {
    title: "Components/Modal",
    decorators: [
      withKnobs
    ],
    component: Modal
  };

export const Dynamic = () => {
  const params = {
    show: boolean("Show Modal", false),
    drag: object("Drag Settings", null),
    className: text("Class Name", "test"),
    title: text("Modal Title", "Modal Example"),
    children: (
      <div>
        <h1>Example</h1>
      </div>
    ),
  }

  const actions = {
    onClose: action('onClose')
  };
  
  return <Modal {...params} {...actions} />
};

export const Draggable = () => {
  const params = {
    show: boolean("Show Modal", false),
    dragSettings: object("Drag Settings", {
      axis: "both",
      handle: ".modal__header",
      defaultPosition: {x: 0, y: 0},
      position: null,
      grid: [25, 25],
      scale: 1,
      onStart: action("Drag Started"),
      onDrag: action("Drag"),
      onStop: action("Drag Stopped")
    }),
    className: text("Class Name", "test"),
    title: text("Modal Title", "Modal Example"),
    children: (
      <div>
        <h1>Example</h1>
      </div>
    ),
  }

  const actions = {
    onClose: action('onClose')
  };
  
  return <Modal {...params} {...actions} />
};