import React from 'react';
import List from './list';
import { withKnobs, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import '../../_stories/stories.scss';

export default {
    title: "Components/List",
    decorators: [
      withKnobs
    ],
    component: List
  };

export const DynamicArray = () => {

  const params = {
    data: object("Data", [
        {
          label: "Choice 1",
          value: 1
        },
        {
          label: "Choice 2",
          value: 2
        },
        {
          label: "Choice 3",
          value: 3
        }
    ])
  }

  const actions = {
    renderItem: function(item, key) {
      action('renderItem', item);
      return (
        <pre>
          key: {key} <br/>
          data: {JSON.stringify(item, undefined, 4)}
        </pre>
      )
    }
  };

  return <List {...params} {...actions} />
};

export const DynamicObject = () => {
  const params = {
    data: object("Data", {

      "Choice 1" : {
        label: "Choice 1",
        value: 1
      },

      "Choice 2" : {
        label: "Choice 2",
        value: 2
      },

      "Choice 3" : {
        label: "Choice 3",
        value: 3
      }
      
    })
  }

  const actions = {
    renderItem: function(item, key) {
      action('renderItem', item);

      return (
        <pre>
          key: {key} <br/>
          data: {JSON.stringify(item, undefined, 4)}
        </pre>
      )
    }
  };
  
  return <List {...params} {...actions} />
};