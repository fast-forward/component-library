import React from 'react';
import PropTypes from 'prop-types';
import "./list.scss";
import {bem}from '../../helpers'

/**
 * Iterate over and render data regardless of structure.
 */
class List extends React.Component {

    render() {
        if (this.props.data) {

            let isArray = Array.isArray(this.props.data);

            return (
                <div className={bem('list', null, [], this.props.className)}>
                    {this.props.renderHeader && 
                        this.props.renderHeader()
                    }

                    {isArray && this.props.data.map( (item, index) => (
                        <React.Fragment>
                            <div key={index} className={bem('list', 'item', [], this.props.listItemClassName)}>
                                {this.props.renderItem(item, index)}
                            </div>
                            {this.props.seperator && index < this.props.data.length - 1 && 
                                <React.Fragment>
                                    {this.props.seperator}
                                </React.Fragment>
                            }
                        </React.Fragment>
                    ))}

                    {!isArray && typeof this.props.data === "object" && Object.keys(this.props.data).map( (key, index) => (

                        <React.Fragment>
                            <div key={key} className={bem('list', 'item', [], this.props.listItemClassName)}>
                                {this.props.renderItem(this.props.data[key], key)}
                            </div>
                            {this.props.seperator && index < Object.keys(this.props.data).length - 1 && 
                                <React.Fragment>
                                    {this.props.seperator}
                                </React.Fragment>
                            }
                        </React.Fragment>
                    ))}


                    {this.props.renderFooter && 
                        this.props.renderFooter()
                    }

                </div>
            )
        } else {
            return null;
        }
    }
}

List.propTypes = {
    className: PropTypes.string,
    listItemClassName: PropTypes.string,
    data: PropTypes.any.isRequired,
    renderItem: PropTypes.func.isRequired
};

List.defaultProps = {
    className: "",
    listItemClassName: "",
    renderHeader: null,
    renderFooter: null,
    seperator: null
}

export default List;
