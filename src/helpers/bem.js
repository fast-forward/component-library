/**
 * block__element--modifier class name helper 
 * @param {string} block block name
 * @param {string} element element name
 * @param {array} modifiers array of modifiers
 * @param {string} append class string to append.usually for tagging instances 
 */
export default function (
    block, 
    element = '', 
    modifiers = [], 
    append = ''
) {
    let be = block.trim()
    if (element) { be += `__${element.trim()}`}
    
    return [
        be,
        ...modifiers
            .filter( m => !!m )
            .map( m => `${be}--${m}`),
        append
    ]
    .filter( m => !!m )
    .join(' ')
}