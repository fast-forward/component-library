import React from 'react';
import './App.scss';
import PickList from './components/pickList/pickList';
import FacetList from "./components/facetList/facetList";
import { FilterList } from './components';
import Animation from './components/animation/animation';
import { TweenMax } from "gsap/all";

export class App extends React.Component {
  
  options = {
    type: {
      label: "Type" ,
      name: "type" ,
      type: "checkbox" ,
      expanded: true ,
      filters: {},
      visibility: ["search", "product"]
    },
    color: {
      label: "Color" ,
      name: "color" ,
      type: "checkbox" ,
      expanded: false ,
      filters: { },
      visibility: ["search", "product"]
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      facetFilters: {
        type: [],
        color: []
      },
      showAnimation: false
    };
  }

  render () {
    let action = function (name) { 
      return (data) => {
        console.log(name, data)
      }
    };

    return (

      <div className="App">
        <h1>Fast Forward - React Component Library</h1>
        
        <button onClick={ event => {
          event.preventDefault(); 
          this.setState({showAnimation: !this.state.showAnimation});
        }}>
          Show
        </button>

        <section>
          <h3>Animation</h3>
          <article>
              <Animation
                className="sub-animation" 
                active={this.state.showAnimation}
                onEntering={ (element, done) => {
                  action("onEnter")(element);

                  TweenMax.to(element, 5, {
                    x: 0,
                    backgroundColor: "#ffffff",
                    onComplete: done
                  });
                }}
                onEntered={action("onEntered")}
                onExiting={ (element, done) => {
                  action("onExiting")(element);

                  TweenMax.to(element, 5, {
                    x: 1000,
                    backgroundColor: "#000000",
                    onComplete: done
                  });

                }}
                onExited={action("onExited")}
              >
                <div>Goodbye earth</div>
              </Animation>
              
          </article>
        </section>

        <section>
          <h3>PickList</h3>
          <article>
            <PickList 
              onAdd={ item => {
                console.log("Added", item);
              }}
              onClose={ () => {
                console.log("Closed");
              }}
              onOpen={ () => {
                console.log("Opened");
              }}
              data={[
                {
                  label: "Choice 1",
                  value: 1
                },
                {
                  label: "Choice 2",
                  value: 2
                },
                {
                  label: "Choice 3",
                  value: 3
                }
              ]} 
            />
          </article>
        </section>
  
        <section>
          <h3>FacetList</h3>
          <article>
            <h4>Horizontal</h4>
            <FacetList 
              horizontal={true}
              onChange={ item => { 
                var faceFilters = this.state.facetFilters;
                if (!this.state.facetFilters[item.name]) {
                  faceFilters[item.name] = [];
                }
    
                if (item.active) {
                  faceFilters[item.name].push(item.value);
                } else {
                  faceFilters[item.name].splice(faceFilters[item.name].indexOf(item.name), 1)
                }

                this.setState({facetFilters: faceFilters})
              }}
              facets={{
                type: {
                  buckets: [
                    {
                      val: "Pants",
                      count: 5
                    },
                    {
                      val: "T-shirts",
                      count: 10
                    },
                    {
                      val: "Socks",
                      count: 15
                    }
                  ]
                },
                color: {
                  buckets: [
                    {
                      val: "Red",
                      count: 5
                    },
                    {
                      val: "Blue",
                      count: 10
                    },
                    {
                      val: "Green",
                      count: 15
                    }
                  ]
                }
              }}
              filters={this.state.facetFilters}
              options={this.options}
            />

            <h4>Verticle</h4>
            <FacetList 
              horizontal={false}
              onChange={ item => { 
                var faceFilters = this.state.facetFilters;
                if (!this.state.facetFilters[item.name]) {
                  faceFilters[item.name] = [];
                }
    
                if (item.active) {
                  faceFilters[item.name].push(item.value);
                } else {
                  faceFilters[item.name].splice(faceFilters[item.name].indexOf(item.name), 1)
                }

                this.setState({facetFilters: faceFilters})
              }}
              facets={{
                type: {
                  buckets: [
                    {
                      val: "Pants",
                      count: 5
                    },
                    {
                      val: "T-shirts",
                      count: 10
                    },
                    {
                      val: "Socks",
                      count: 15
                    }
                  ]
                },
                color: {
                  buckets: [
                    {
                      val: "Red",
                      count: 5
                    },
                    {
                      val: "Blue",
                      count: 10
                    },
                    {
                      val: "Green",
                      count: 15
                    }
                  ]
                }
              }}
              filters={this.state.facetFilters}
              options={this.options}
            />
          </article>
        </section>

        <section>
          <h3>FilterList</h3>
          <article>
            <FilterList 
              horizontal={false}
              onRemove={ item => { 
                var facetFilters = this.state.facetFilters;
                if (!facetFilters[item.name]) {
                  facetFilters[item.name] = [];
                }
                facetFilters[item.name].splice(facetFilters[item.name].indexOf(item.value), 1)
                this.setState({facetFilters: facetFilters})
              }}
              onClear={ () => this.setState({facetFilters: {}}) }
              filters={this.state.facetFilters}
              options={this.options}
            />
          </article>
        </section>
      </div>
    );
  }
}

export default App;
