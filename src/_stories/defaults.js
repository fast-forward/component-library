export const facetOptions = {
    type: {
      label: "Type" ,
      name: "type" ,
      type: "checkbox" ,
      expanded: true ,
      filters: {},
      visibility: ["search", "product"]
    },
    color: {
      label: "Color" ,
      name: "color" ,
      type: "checkbox" ,
      expanded: false ,
      filters: { },
      visibility: ["search", "product"]
    }
  };
  
  export const filters = {
    type: [
        "Pants"
    ],
    color: [
      "Blue"
    ]
  }

  export const facets = {
    type: {
      buckets: [
        {
          val: "Pants",
          count: 5
        },
        {
          val: "T-shirts",
          count: 10
        },
        {
          val: "Socks",
          count: 15
        }
      ]
    },
    color: {
      buckets: [
        {
          val: "Red",
          count: 5
        },
        {
          val: "Blue",
          count: 10
        },
        {
          val: "Green",
          count: 15
        }
      ]
    }
  }